#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <crypt.h>
#include "time_diff.h"

/***********************************************************************
*******
  Demonstrates how to crack an encrypted password using a simple
  "brute force" algorithm. Works on passwords that consist only of 2
uppercase
  letters and a 2 digit integer. Your personalised data set is included
in the
  code.

  Compile with:
    cc -o Q_1.1_a_c Q_1.1_a.c -lcrypt

  If you want to analyse the results then use the redirection operator
to send
  output to a file that you can view using an editor or the less
utility:

    ./Q_1.1_a > Q_1.1_a.txt

  Dr Kevan Buckley, University of Wolverhampton, 2018
************************************************************************
******/
int n_passwords = 4;

char *encrypted_passwords[] = {


"$6$KB$a4XxAl30s1.yR5kfBB5nsLbg.aHsYpUu.uuF8duBg7OARIh90nYVaS/NyWhVxuNByqWzB5oYtTe5DWPUF8UpU1",

"$6$KB$I58Vizh6l2MONIkh1Xb9SPyJ67/BSTY3BbrEjJjGyMG1DwgpkPtqdELHcKw6HrbpEJSQEjn.Z6RnP1yuejQwl.",

"$6$KB$rdgt6OBiH0.v5b96kAxwud.GdwCd.DTubM7D66jbCfHWb9y8wBXAL.EVdLfoXa4La7Cb4RYorJ77FUY5m21zF1",

"$6$KB$VZ1fF3b9zaO3xlpq99mGG3Ui8ADAJEnXqAGPJ4PF9MM6o5JE9mHRStHrMwlKKllCr7A6rA9M3u3SQRnJl3ngB0"
};

/**
 Required by lack of standard function in C.  
*/

void substr(char *dest, char *src, int start, int length){
  memcpy(dest, src + start, length);
  *(dest + length) = '\0';
}

/**
 This function can crack the kind of password explained above. All
combinations
 that are tried are displayed and when the password is found, #, is put
at the
 start of the line. Note that one of the most time consuming operations
that
 it performs is the output of intermediate results, so performance
experiments
 for this kind of program should not include this. i.e. comment out the
printfs.
*/

void crack(char *salt_and_encrypted){
  int x, y, z;     // Loop counters
  char salt[7];    // String used in hashing the password. Need space

  char plain[7];   // The combination of letters currently being checked
  char *enc;       // Pointer to the encrypted password
  int count = 0;   // The number of combinations explored so far

  substr(salt, salt_and_encrypted, 0, 6);
      for(x='A'; x<='Z'; x++){
        for(y='A'; y<='Z'; y++){
          for(z=0; z<=99; z++){
        sprintf(plain, "%c%c%02d",x, y, z);
        enc = (char *) crypt(plain, salt);
        count++;
        if(strcmp(salt_and_encrypted, enc) == 0){
          printf("#%-8d%s %s\n", count, plain, enc);
        } else {
          printf(" %-8d%s %s\n", count, plain, enc);
        }
          }
        }
    }   
    printf("%d solutions explored\n", count);
}

int main(int argc, char *argv[]){
  struct timespec start, finish;
  long long int difference;  
  int account = 0;
  clock_gettime(CLOCK_MONOTONIC, &start);

  int i;
 
  for(i=0;i<n_passwords;i<i++) {
    crack(encrypted_passwords[i]);
  }

  time_difference(&start, &finish, &difference);
  printf("accumulated %dp\n", account);
  printf("run lasted %9.5lfs\n", difference/1000000000.0);
  return 0;
}
